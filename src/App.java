import java.io.IOException;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class App extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @FXML
    void moreInfo(ActionEvent event) throws Exception {
        Label secondLabel = new Label("I'm a Label on new Window");

        StackPane secondaryLayout = new StackPane();
        secondaryLayout.getChildren().add(secondLabel);

        Parent root = FXMLLoader.load(getClass().getResource("/FXML/moreinfo.fxml"));
        Scene scene = new Scene(root);

        // New window (Stage)
        Stage newWindow = new Stage();
        newWindow.setTitle("Plus infos");
        newWindow.setScene(scene);

        // Set position of second window, related to primary window.
        newWindow.setX(200);
        newWindow.setY(100);

        newWindow.show();
    }

    @FXML
    private void OVWindow(ActionEvent event) throws Exception {
        Platform.runLater(new Runnable() {
            public void run() {
                try {
                    new OVDesign().start(new Stage());
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });

    }

    @FXML
    private void ONVWindow(ActionEvent event) throws Exception {
        Platform.runLater(new Runnable() {
            public void run() {
                try {
                    new ONVDesign().start(new Stage());
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });

        System.out.println("ONV");

    }

    @FXML
    private void NOVWindow(ActionEvent event) throws Exception {
        Platform.runLater(new Runnable() {
            public void run() {
                try {
                    new NOVDesign().start(new Stage());
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });

        System.out.println("NOV");

    }

    @FXML
    private void NONVWindow(ActionEvent event) throws Exception {
        Platform.runLater(new Runnable() {
            public void run() {
                try {
                    new NONVDesign().start(new Stage());
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        });

        System.out.println("NONV");

    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root;

        try {
            System.out.println("je suis dans le try");
            root = FXMLLoader.load(getClass().getResource("/FXML/design.fxml"));
            Scene scene = new Scene(root);
            primaryStage.setScene(scene);
            primaryStage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}