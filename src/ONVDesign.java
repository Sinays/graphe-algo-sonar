import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

import java.io.IOException;
import java.util.ArrayList;

import javafx.application.Application;
import javafx.application.Platform;

import static javafx.application.Application.launch;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.*;

public class ONVDesign extends Application {

    @Struct
    public class Point {
        private double num;
        private Circle c;
        private Label l;
        private Label rang = null;

        public Point(Circle c, double num, Label l) {
            this.c = c;
            this.num = num;
            this.l = l;
        }

        public void setRangLabel(Label rangLabel) {
            this.rang = rangLabel;
        }

        public Label getLabelRang() {
            return this.rang;
        }

        public Label getLabel() {
            return this.l;
        }

        public Circle getCircle() {
            return this.c;
        }

        public double getX() {
            return this.c.getCenterX();
        }

        public double getY() {
            return this.c.getCenterY();
        }

        public double getRadius() {
            return this.c.getRadius();
        }

        public double getNum() {
            return this.num;
        }
    }

    public class ArrowFX {
        private Point pDepart, pArrive;
        private Arrow arc;

        public ArrowFX(Point pDepart, Point pArrive, Arrow arc) {
            this.pDepart = pDepart;
            this.pArrive = pArrive;
            this.arc = arc;
        }

        public Point getDepart() {
            return this.pDepart;
        }

        public Point getArrive() {
            return this.pArrive;
        }

        public Arrow getArrow() {
            return this.arc;
        }

        public double getDepartNum() {
            return this.pDepart.getNum();
        }

        public double getArriveNum() {
            return this.pArrive.getNum();
        }

        public double getXDepart() {
            return this.pDepart.getX();
        }

        public double getYDepart() {
            return this.pDepart.getY();
        }

        public double getXArrive() {
            return this.pArrive.getX();
        }

        public double getYArrive() {
            return this.pArrive.getY();
        }

    }

    private Pane root;
    private Circle circle;
    private ArrayList<Point> aP = new ArrayList<Point>(); // pour les points
    private ArrayList<ArrowFX> aA = new ArrayList<ArrowFX>(); // pour les arcs
    private ArrayList<Point> NumArc = new ArrayList<Point>(); // entre deux points

    private ArrowFX CurrentArc;
    private Point CurrentPoint;
    private Point Depart;
    private Point Arrive;

    @FXML
    private Button poursuivre;

    // @FXML
    // private TextField textCout;

    // true si le point possede des arcs
    public Boolean PointHasArrow(Point p) {
        Boolean b = false;
        for (int i = 0; i < this.aA.size(); i++) {
            ArrowFX a = this.aA.get(i);
            if (a.getDepart().getNum() == p.getNum() || a.getArrive().getNum() == p.getNum()) {
                b = true;
            }
        }
        return b;
    }

    public void DeleteAllArrowPoint(Point p) {
        for (int i = 0; i < this.aA.size(); i++) {
            ArrowFX a = this.aA.get(i);
            if (a.getDepart().getNum() == p.getNum() || a.getArrive().getNum() == p.getNum()) {
                this.aA.remove(i);
                root.getChildren().remove(a.getArrow());
            }
        }
    }

    public void DeleteLastArrow() {
        // Calculate index of last element
        ArrowFX af = this.aA.get(this.aA.size() - 1);
        // Delete last element by passing index
        this.aA.remove(af);
        root.getChildren().remove(af.getArrow());

    }

    public void DeleteLastPoint() {
        // if point has arrow need to delete arrow who touch point
        Point p = this.aP.get(this.aP.size() - 1);
        System.out.println(" Le dernier point est : " + p.getNum());
        System.out.println(PointHasArrow(p));
        while (PointHasArrow(p)) {
            DeleteAllArrowPoint(p);
            this.aP.remove(p);
            root.getChildren().remove(p.getCircle());
            root.getChildren().remove(p.getLabel());
        }
        if (!PointHasArrow(p)) {
            this.aP.remove(p);
            root.getChildren().remove(p.getCircle());
            root.getChildren().remove(p.getLabel());
        }
    }

    public void changeDirectionArrow(Point D, Point A) {
        int k = 0;
        for (int i = 0; i < this.aA.size(); i++) {
            ArrowFX a = this.aA.get(i);
            if (a.getDepart().getNum() == D.getNum() && a.getArrive().getNum() == A.getNum()) {
                k = i;
                System.out.println(
                        " le numero du depart donnée est : " + D.getNum() + " " + " on a : " + a.getDepart().getNum());
                System.out.println(
                        " le numero du arrive donnée est : " + A.getNum() + " " + " on a : " + a.getArrive().getNum());
                root.getChildren().remove(this.aA.get(k).getArrow());
            }
        }

        // change la direction de l'arrete numero k;
        Point DepartAncien = this.aA.get(k).getDepart();
        Point ArriveAncien = this.aA.get(k).getArrive();
        Arrow Arc = new Arrow(true, Color.BLUE);

        ArrowFX f = new ArrowFX(ArriveAncien, DepartAncien, Arc);
        this.aA.set(k, f);
        root.getChildren().add(Arc);

        Arc.setStartX(Arrive.getX());
        Arc.setStartY(Arrive.getY());

        Arc.setEndX(Depart.getX());
        Arc.setEndY(Depart.getY());

    }

    public void clearWindow() {
        for (int i = 0; i < this.aP.size(); i++) {
            Point p = this.aP.get(i);
            root.getChildren().remove(p.getCircle());
            root.getChildren().remove(p.getLabel());
            if (p.getLabelRang() != null) {
                root.getChildren().remove(p.getLabelRang());
            }
        }

        // delete all line
        for (int i = 0; i < this.aA.size(); i++) {
            ArrowFX a = this.aA.get(i);
            root.getChildren().remove(a.getArrow());
        }

        this.aA.clear();
        this.aP.clear();
    }

    public void onEnter(KeyEvent event) throws Exception {
        System.out.println("dedands");

        if (this.aP.size() == 0 || this.aA.size() == 0) {
            WarningBox.display("Warning", "veuillez d'abord saisir un graphe avec des sommets et des arretes ! ");
        } else {

            // on calcul le graphe
            int nbPoint = this.aP.size();
            int nbArc = this.aA.size();
            int[][] mat_adj = new int[nbPoint + 1][nbPoint + 1];

            mat_adj[0][0] = nbPoint;
            mat_adj[0][1] = nbArc;

            for (int i = 0; i < nbArc; i++) {
                double numdepart = this.aA.get(i).getDepartNum();
                double numArrive = this.aA.get(i).getArriveNum();

                mat_adj[(int) Math.round(numdepart)][(int) Math.round(numArrive)] = 1;

            }

            // creation du graphe OV
            GrapheONV g = new GrapheONV(mat_adj);

            String menu = "";
            menu += "Veuillez donner le nom de l'algorithme ou le nom de la fonction a traiter : " + "\n";
            menu += "rang" + "\n";
            menu += "distance" + "\n";
            menu += "Pred" + "\n";
            menu += "FS APS" + "\n";
            menu += "FPP APP" + "\n";
            menu += "Clear" + "\n";
            menu += "Delete Last Point" + "\n";
            menu += "Delete Last Arrow" + "\n";
            menu += "Change Direction" + "\n";
            menu += "Load file" + "\n";
            menu += "Save file" + "\n";
            // return g;
            String result = confirmBox.display("Menu Graphe oriente Non Value", menu);
            switch (result) {
                case "Save file":
                    String fileSave = CoutBox.display("Nom de fichier ",
                            "Veuillez saisir le nom des fichiers dans l'ordre suivant pour le sauvegarde: mat_adj  ou fs aps  (séparer par des espaces) : "
                                    + "\n" + "exemple : mat.txt fs.txt aps.txt");
                    String[] fileSaveArray = fileSave.split("\\s+");

                    if (fileSaveArray.length == 2) {
                        g.SaveIntoFileFSWithoutCout(fileSaveArray[0], fileSaveArray[1]);
                    }

                    if (fileSaveArray.length == 1) {
                        g.SaveIntoFileMat(fileSaveArray[0]);
                    }

                    clearWindow();
                    // delete all circle
                    for (int i = 0; i < this.aP.size(); i++) {
                        Point p = this.aP.get(i);
                        root.getChildren().remove(p.getCircle());
                        root.getChildren().remove(p.getLabel());
                    }

                    // delete all line
                    for (int i = 0; i < this.aA.size(); i++) {
                        ArrowFX a = this.aA.get(i);
                        root.getChildren().remove(a.getArrow());
                    }

                    this.aA.clear();
                    this.aP.clear();

                    break;
                case "Load file":
                    clearWindow();
                    String file = CoutBox.display("Nom de fichier ",
                            "Veuillez saisir le nom des fichiers dans l'ordre suivant pour le chargement: mat_adj ou fs aps  (séparer par des espaces) : "
                                    + "\n" + "exemple : mat.txt fs.txt aps.txt");
                    String[] fileArray = file.split("\\s+");

                    if (fileArray.length == 2) {
                        // mat_cout fs aps
                        g.filefs(fileArray[0], fileArray[1]);
                    }

                    if (fileArray.length == 1) {
                        // mat_adj mat_cout
                        g.file(fileArray[0]);
                    }

                    // delete all circle
                    for (int i = 0; i < this.aP.size(); i++) {
                        Point p = this.aP.get(i);
                        root.getChildren().remove(p.getCircle());
                        root.getChildren().remove(p.getLabel());
                    }

                    // delete all line
                    for (int i = 0; i < this.aA.size(); i++) {
                        ArrowFX a = this.aA.get(i);
                        root.getChildren().remove(a.getArrow());
                    }

                    this.aA.clear();
                    this.aP.clear();

                    // generation de point aleatoire sur la page

                    int nbSommet = g.mat_adj[0][0];

                    for (int i = 0; i < nbSommet; i++) {
                        double x = 100 + Math.random() * (700 - 100);
                        double y = 100 + Math.random() * (700 - 100);

                        circle = new Circle(x, y, 0.0, Color.GREEN);
                        circle.setStroke(Color.GREEN);
                        circle.setStrokeWidth(2.0);
                        circle.setRadius(30);
                        root.getChildren().add(circle);

                        Label label = new Label(String.valueOf(i + 1));
                        Font font = Font.font("Brush Script MT", FontWeight.NORMAL, 20);
                        label.setFont(font);
                        label.setTextFill(Color.BLACK);

                        label.setTranslateX(x);
                        label.setTranslateY(y);

                        CurrentPoint = new Point(circle, aP.size() + 1, label);
                        this.aP.add(CurrentPoint);
                        root.getChildren().add(label);

                    }

                    for (int i = 1; i <= nbSommet; i++) {
                        for (int j = 1; j <= nbSommet; j++) {
                            if (g.mat_adj[i][j] == 1) {

                                Point Depart = PointofNum(i);
                                Point Arrive = PointofNum(j);

                                Arrow arrow = new Arrow(true, Color.BLUE);
                                root.getChildren().add(arrow);

                                arrow.setStartX(Depart.getX());
                                arrow.setStartY(Depart.getY());

                                arrow.setEndX(Arrive.getX());
                                arrow.setEndY(Arrive.getY());

                                double milieuX = (Arrive.getX() + Depart.getX()) / 2;
                                double milieuY = (Arrive.getY() + Depart.getY()) / 2;

                                CurrentArc = new ArrowFX(Depart, Arrive, arrow);
                                NumArc.clear();
                                this.aA.add(CurrentArc);
                            }
                        }
                    }

                    break;

                case "Change Direction":
                    String direction = CoutBox.display("Changement de direction ",
                            "Veuillez saisir le numéro du départ et celui de l'arrivé (séparer par des espaces) : "
                                    + "\n" + "exemple : 1 2");
                    String[] splited = direction.split("\\s+");
                    while (splited.length != 2) {
                        direction = CoutBox.display("Changement de direction ",
                                "Veuillez saisir le numéro du départ et celui de l'arrivé (séparer par des espaces) : "
                                        + "\n" + "exemple : 1 2");
                    }

                    Point D = PointofNum(Integer.parseInt(splited[0]));
                    Point A = PointofNum(Integer.parseInt(splited[1]));
                    changeDirectionArrow(D, A);

                    break;
                case "Delete Last Point":
                    DeleteLastPoint();
                    break;
                case "Delete Last Arrow":
                    DeleteLastArrow();
                    break;
                case "Clear":
                    clearWindow();
                    break;
                case "distance":
                    // code block
                    // affichage matrice distance

                    int[][] dist = g.distPrincipale();

                    String resultatDistance = "";
                    resultatDistance += "distance[] : ";
                    resultatDistance += "\n";
                    for (int i = 0; i < dist.length; i++) {
                        for (int j = 0; j < dist.length; j++) {
                            resultatDistance += dist[i][j] + " ";
                        }
                        resultatDistance += "\n";
                    }
                    printBox.display("Algorithme Distance (Parcours Largeur)", resultatDistance);

                    break;
                case "rang":
                    // code block
                    System.out.print("J'execute la fonction rang");
                    int[] rang = g.Rang();
                    // ajoute le numero sur le point
                    for (int i = 0; i < rang.length; i++) {
                        double x = this.aP.get(i).getX();
                        double y = this.aP.get(i).getY();

                        int r = rang[(int) Math.round(this.aP.get(i).getNum())];

                        Label label = new Label(String.valueOf(r));
                        Font font = Font.font("Brush Script MT", FontWeight.NORMAL, 20);
                        label.setFont(font);
                        label.setTextFill(Color.RED);

                        label.setTranslateX(x + 20);
                        label.setTranslateY(y + 20);
                        this.aP.get(i).setRangLabel(label);
                        root.getChildren().add(label);
                    }
                    break;
                case "Pred":
                    // on donne le sommet et on calcul le nombre de predecesseur de ce sommet
                    String sommet = CoutBox.display("Sommet de depart ",
                            "Veuillez Saisir le numero du sommet de depart : ");

                    int s = Integer.valueOf(sommet);

                    String affichage = "Le nombre de predecesseur du sommet " + s + " est : ";
                    int nbpred = 0;
                    int k = g.app[s];
                    int[] fp = g.getFp();
                    while (fp[k] != 0) {
                        k++;
                        nbpred++;
                    }
                    affichage += nbpred;
                    printBox.display("Nombre de predecesseur", affichage);

                    System.out.print("Pred");
                    break;
                case "FS APS":
                    // code block
                    int[] fs = g.getFs();
                    int[] aps = g.getAps();

                    String resultat = "";
                    resultat += "fs[] : ";

                    for (int i = 0; i < fs.length; i++) {
                        resultat += fs[i] + " ";
                    }
                    resultat += "\n";
                    resultat += "aps[] : ";
                    for (int i = 0; i < aps.length; i++) {
                        resultat += aps[i] + " ";
                    }
                    resultat += "\n";
                    printBox.display("FS APS", resultat);

                    System.out.print("FS APS");
                    break;

                case "FPP APP":
                    // code block
                    int[] fpp = g.getFp();
                    int[] app = g.getApp();

                    String resultfpp = "";
                    resultfpp += "fp[] : ";
                    resultfpp += "\n";

                    for (int i = 0; i < fpp.length; i++) {
                        resultfpp += fpp[i] + " ";
                    }
                    resultfpp += "\n";
                    resultfpp += "app[] : ";
                    for (int i = 0; i < app.length; i++) {
                        resultfpp += app[i] + " ";
                    }
                    resultfpp += "\n";
                    printBox.display("FP APP", resultfpp);
                    System.out.print("FPP APP");
                    break;
                default:
                    // code block
                    System.out.print("rien");
            }
        }
    }

    // @FXML
    // void rangBtn(ActionEvent event) {
    // System.out.println(this.fini);
    // System.out.println("je vais modifier les rangs des sommets : ");

    // }

    @FXML
    private void handleConnectButtonAction(ActionEvent event) {
        poursuivre.setDisable(true);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {

        root = FXMLLoader.load(getClass().getResource("/FXML/ONV.fxml"));

        root.setOnMousePressed(arg0 -> {
            try {
                handleMousePressed(arg0);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        });

        root.setOnKeyPressed(arg0 -> {
            try {
                onEnter(arg0);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        });

        primaryStage.setScene(new Scene(root, 1000.0, 650.0));
        primaryStage.show();
    }

    // fonction aide
    public double numOfPoint(double X, double Y) {
        double result = 0;
        for (int i = 0; i < this.aP.size(); i++) {
            double distance = Math
                    .sqrt(Math.pow(X - this.aP.get(i).getX(), 2.0) + Math.pow(Y - this.aP.get(i).getY(), 2.0));
            if (distance <= this.aP.get(i).getRadius()) {
                result = this.aP.get(i).getNum();
            }
        }

        return result;
    }

    // fonction aide
    public Point PointofNum(double num) {
        for (int i = 0; i < this.aP.size(); i++) {
            if (this.aP.get(i).getNum() == num) {
                return this.aP.get(i);
            }
        }
        return null;
    }

    // fonction aide
    public Boolean PointExist(double X, double Y) {
        for (int i = 0; i < this.aP.size(); i++) {
            double distance = Math
                    .sqrt(Math.pow(X - this.aP.get(i).getX(), 2.0) + Math.pow(Y - this.aP.get(i).getY(), 2.0));
            if (distance <= this.aP.get(i).getRadius()) {
                return true;
            }
        }
        return null;
    }

    // chaque clique sur le root
    private void handleMousePressed(MouseEvent e) throws IOException {

        if (e.getButton() == MouseButton.PRIMARY && e.getClickCount() == 1) {
            if (circle == null) {
                // start drawing a new circle
                circle = new Circle(e.getX(), e.getY(), 0.0, Color.ORANGE);
                circle.setStroke(Color.RED);
                circle.setStrokeWidth(2.0);
                circle.setRadius(30);
                root.getChildren().add(circle);

                Label label = new Label(String.valueOf(aP.size() + 1));
                Font font = Font.font("Brush Script MT", FontWeight.NORMAL, 20);
                label.setFont(font);
                label.setTextFill(Color.BLACK);

                label.setTranslateX(e.getX());
                label.setTranslateY(e.getY());

                CurrentPoint = new Point(circle, aP.size() + 1, label);
                this.aP.add(CurrentPoint);
                root.getChildren().add(label);
                circle = null;

            }
        }

        if (e.getButton() == MouseButton.SECONDARY) {
            System.out.println("trace line");
            Boolean b = PointExist(e.getX(), e.getY());
            if (this.aP.size() == 0) {
                WarningBox.display("Attention",
                        "Veuillez créer d'abord des points pour ensuite pouvoir tracer tous vos traits");
            } else if (b == null) {
                WarningBox.display("Attention", "Veuillez cliquer sur des boutons");
                NumArc.clear();
            } else {
                if (b) {
                    double numero = numOfPoint(e.getX(), e.getY());
                    Point p = PointofNum(numero);
                    NumArc.add(p);
                    if (NumArc.size() == 2) {

                        this.Depart = NumArc.get(0);
                        this.Arrive = NumArc.get(1);

                        // on trace l'arc en question
                        Arrow arrow = new Arrow(true, Color.BLUE);
                        root.getChildren().add(arrow);

                        arrow.setStartX(Depart.getX());
                        arrow.setStartY(Depart.getY());

                        arrow.setEndX(Arrive.getX());
                        arrow.setEndY(Arrive.getY());

                        CurrentArc = new ArrowFX(Depart, Arrive, arrow);
                        NumArc.clear();
                        this.aA.add(CurrentArc);
                    }
                }
            }

        }

        if (e.getButton() == MouseButton.MIDDLE) {
            System.out.println("middle");
            System.out.println("voici la taille de aA : " + aA.size());
            for (int i = 0; i < this.aA.size(); i++) {
                System.out.println("VOICI L'arc " + i);
                System.out.println("Depart Numero : " + this.aA.get(i).getDepartNum());
                System.out.println("Arrive Numero : " + this.aA.get(i).getArriveNum());
                System.out.println(" ");
            }
        }

    }

    public static void main(String args[]) {
        launch(args);
    }
}
